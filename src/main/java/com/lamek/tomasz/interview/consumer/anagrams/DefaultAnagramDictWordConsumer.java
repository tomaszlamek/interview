package com.lamek.tomasz.interview.consumer.anagrams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultAnagramDictWordConsumer implements AnagramDictWordConsumer {

    Map<String, List<String>> anagramsListMap = new TreeMap<>();

    Collection<? extends Collection<String>> anagrams;

    @Override
    public void consumeWord(String word) {
        if (word != null && !word.isEmpty()) {
            String key = generateKeyForWord(word);
            addWord(word, key);
        }
    }

    private String generateKeyForWord(String word) {
        char[] wordChars = word.toLowerCase().toCharArray();
        Arrays.sort(wordChars);
        return String.valueOf(wordChars);
    }

    private void addWord(String word, String key) {
        List<String> words = anagramsListMap.computeIfAbsent(key, s -> new LinkedList<>());
        words.add(word);
    }

    @Override
    public Collection<? extends Collection<String>> getAnagrams() {
        if (anagrams == null)
            anagrams = extractAnagrams();
        return anagrams;
    }

    private List<List<String>> extractAnagrams() {
        return filter().collect(Collectors.toList());
    }

    private Stream<List<String>> filter() {
        return getStream().filter(strings -> strings.size() > 1);
    }

    private Stream<List<String>> getStream() {
        return anagramsListMap.values().stream();
    }


}