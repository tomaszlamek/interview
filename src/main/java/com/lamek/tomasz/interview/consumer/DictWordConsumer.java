package com.lamek.tomasz.interview.consumer;

public interface DictWordConsumer {
    void consumeWord(String word);
}
