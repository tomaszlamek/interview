package com.lamek.tomasz.interview.consumer.anagrams;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;

import java.util.Collection;

public interface AnagramDictWordConsumer extends DictWordConsumer {
    Collection<? extends Collection<String>> getAnagrams();
}
