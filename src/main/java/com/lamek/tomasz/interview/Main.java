package com.lamek.tomasz.interview;

import com.lamek.tomasz.interview.analyzer.DictAnalyzer;
import com.lamek.tomasz.interview.analyzer.DictAnalyzerBuilder;
import com.lamek.tomasz.interview.analyzer.DictAnalyzerBuilderWithConsumer;
import com.lamek.tomasz.interview.consumer.anagrams.DefaultAnagramDictWordConsumer;
import com.lamek.tomasz.interview.interpreter.anagrams.AnagramsPrinter;
import com.lamek.tomasz.interview.interpreter.anagrams.LongestAnagramsFinder;
import com.lamek.tomasz.interview.interpreter.anagrams.MostOccurredAnagramFinder;
import com.lamek.tomasz.interview.provider.DictDataFromFromUrlProvider;

public class Main {

    public static final String DICT_URL = "http://codekata.com/data/wordlist.txt";

    public static void main(String[] args) {

        DictAnalyzer dictAnalyzer = constructDictReader();

        dictAnalyzer.analyzeDict();

    }

    private static DictAnalyzer constructDictReader() {

        DictAnalyzerBuilderWithConsumer analyzerBuilder = DictAnalyzerBuilder.getDictAnalyzerBuilder();

        return analyzerBuilder.
                setConsumer(new DefaultAnagramDictWordConsumer()).
                setDataProvider(new DictDataFromFromUrlProvider(DICT_URL, "cp1250")).
                addInterpreter(new AnagramsPrinter()).
                addInterpreter(new LongestAnagramsFinder()).
                addInterpreter(new MostOccurredAnagramFinder()).
                getAnalyzer();
    }

}
