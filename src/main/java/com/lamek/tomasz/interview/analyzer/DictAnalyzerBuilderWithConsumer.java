package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;

public interface DictAnalyzerBuilderWithConsumer {
    DictAnalyzerBuilderWithDataProvider setConsumer(DictWordConsumer consumer);
}
