package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.provider.DictDataProvider;

public interface DictAnalyzerBuilderWithDataProvider {
    DictAnalyzerBuilderWithOptionalInterpreter setDataProvider(DictDataProvider provider);
}
