package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.interpreter.Interpreter;


public interface DictAnalyzerBuilderWithInterpreter {
    DictAnalyzerBuilderWithOptionalInterpreter addInterpreter(Interpreter interpreter);
}
