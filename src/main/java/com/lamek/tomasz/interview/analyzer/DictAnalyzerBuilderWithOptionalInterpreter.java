package com.lamek.tomasz.interview.analyzer;

public interface DictAnalyzerBuilderWithOptionalInterpreter extends DictAnalyzerBuilderWithInterpreter {
    DictAnalyzer getAnalyzer();
}
