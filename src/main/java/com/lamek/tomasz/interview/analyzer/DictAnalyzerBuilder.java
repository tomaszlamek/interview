package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;
import com.lamek.tomasz.interview.interpreter.Interpreter;
import com.lamek.tomasz.interview.provider.DictDataProvider;

public class DictAnalyzerBuilder implements DictAnalyzerBuilderWithConsumer, DictAnalyzerBuilderWithDataProvider, DictAnalyzerBuilderWithInterpreter, DictAnalyzerBuilderWithOptionalInterpreter {

    private DictAnalyzer dictAnalyzer = new DictAnalyzer();

    private DictAnalyzerBuilder() {
    }

    public static DictAnalyzerBuilderWithConsumer getDictAnalyzerBuilder() {
        return new DictAnalyzerBuilder();
    }

    @Override
    public DictAnalyzerBuilderWithDataProvider setConsumer(DictWordConsumer consumer) {
        dictAnalyzer.setWordConsumer(consumer);
        return this;
    }

    @Override
    public DictAnalyzerBuilderWithOptionalInterpreter setDataProvider(DictDataProvider provider) {
        dictAnalyzer.setDictDataProvider(provider);
        return this;
    }

    @Override
    public DictAnalyzerBuilderWithOptionalInterpreter addInterpreter(Interpreter interpreter) {
        dictAnalyzer.addInterpreter(interpreter);
        return this;
    }

    @Override
    public DictAnalyzer getAnalyzer() {
        return dictAnalyzer;
    }
}
