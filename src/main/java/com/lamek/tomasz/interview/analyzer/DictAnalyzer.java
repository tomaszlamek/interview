package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;
import com.lamek.tomasz.interview.interpreter.Interpreter;
import com.lamek.tomasz.interview.provider.DictDataProvider;

import java.util.LinkedList;
import java.util.List;

public class DictAnalyzer {

    private static final String ILLEGAL_STATE_MESSAGE = "dictDataProvider and wordConsumer must be provided!";

    private DictWordConsumer wordConsumer;
    private DictDataProvider dataProvider;
    private List<Interpreter> interpreters = new LinkedList<>();

    DictAnalyzer() {
    }

    public void analyzeDict() {

        checkState();

        readDict();

        interpretResults();
    }

    private void readDict() {
        for (String word : dataProvider) {
            wordConsumer.consumeWord(word);
        }
    }

    private void interpretResults() {
        for (Interpreter interpreter : interpreters) {
            interpreter.interpretResults(wordConsumer);
        }
    }

    private void checkState() {

        if (wordConsumer == null || dataProvider == null) {
            throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
        }

    }

    public void setWordConsumer(DictWordConsumer wordConsumer) {
        this.wordConsumer = wordConsumer;
    }

    public void setDictDataProvider(DictDataProvider dictDataProvider) {
        this.dataProvider = dictDataProvider;
    }

    public void addInterpreter(Interpreter interpreter) {
        interpreters.add(interpreter);
    }
}
