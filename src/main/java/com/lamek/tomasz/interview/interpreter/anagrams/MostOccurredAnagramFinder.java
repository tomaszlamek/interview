package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

public class MostOccurredAnagramFinder implements AnagramsInterpreter {

    private static final String MOST_OCCURRED_ANAGRAM = "\nThe most occurred anagram :\n %s\n";

    @Override
    public void interpretResults(AnagramDictWordConsumer consumer) {

        Collection<String> anagram = getMostOccurredAnagram(consumer.getAnagrams());
        String anagramsAsString = Arrays.toString(anagram.toArray());
        print(anagramsAsString);
    }

    private void print(String anagramsAsString) {
        System.out.println(String.format(MOST_OCCURRED_ANAGRAM, anagramsAsString));
    }

    Collection<String> getMostOccurredAnagram(Collection<? extends Collection<String>> anagrams) {
        Collection<String> mostOccurredAnagram = getMostOccurredAnagramOrNull(anagrams);
        if(mostOccurredAnagram == null)
            mostOccurredAnagram = new LinkedList<>();
        return mostOccurredAnagram;
    }

    private Collection<String> getMostOccurredAnagramOrNull(Collection<? extends Collection<String>> anagrams) {
        return anagrams.stream().max(this::compare).orElse(null);
    }

    private int compare(Collection<String> c1, Collection<String> c2) {
        return Integer.compare(c1.size(), c2.size());
    }
}
