package com.lamek.tomasz.interview.interpreter;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;

public interface Interpreter<T extends DictWordConsumer> {
    void interpretResults(T results);
}
