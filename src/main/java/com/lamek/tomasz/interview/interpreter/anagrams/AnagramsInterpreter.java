package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;
import com.lamek.tomasz.interview.interpreter.Interpreter;

public interface AnagramsInterpreter extends Interpreter<AnagramDictWordConsumer> {

}
