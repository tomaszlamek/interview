package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Stream;

public class LongestAnagramsFinder implements AnagramsInterpreter {
    private static final String LONGEST_ANAGRAM = "\nThe longest anagram:\n%s\n";

    @Override
    public void interpretResults(AnagramDictWordConsumer consumer) {
        Collection<String> anagram = getBiggestAnagramsCollection(consumer);
        print(anagram);
    }

    private void print(Collection<String> anagram) {
        System.out.println(String.format(LONGEST_ANAGRAM, getAsString(anagram)));
    }

    private String getAsString(Collection<String> anagram) {
        return Arrays.toString(getAsArray(anagram));
    }

    Collection<String> getBiggestAnagramsCollection(AnagramDictWordConsumer consumer) {
        Collection<String> biggestCollection = getBiggestAnagramCollectionOrNull(consumer);
        if(biggestCollection == null)
            biggestCollection = new LinkedList<>();
        return biggestCollection;
    }

    private Collection<String> getBiggestAnagramCollectionOrNull(AnagramDictWordConsumer consumer) {
        return getStream(consumer).max(this::compare).orElse(null);
    }

    private int compare(Collection<String> c1, Collection<String> c2) {
        return Integer.compare(getFirstElementLength(c1), getFirstElementLength(c2));
    }

    private int getFirstElementLength(Collection<String> c) {
        return (getAsArray(c)[0]).length();
    }

    private String[] getAsArray(Collection<String> c) {
        return c.toArray(new String[]{});
    }

    private Stream<? extends Collection<String>> getStream(AnagramDictWordConsumer consumer) {
        return consumer.getAnagrams().stream();
    }
}
