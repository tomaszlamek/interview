package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;

import java.util.Arrays;
import java.util.Collection;

public class AnagramsPrinter implements AnagramsInterpreter {

    @Override
    public void interpretResults(AnagramDictWordConsumer consumer) {
        printAnagramsCollections(consumer.getAnagrams());
    }

    private void printAnagramsCollections(Collection<? extends Collection<String>> anagrams) {
        anagrams.parallelStream().forEach(this::printAnagramsCollection);
    }

    private void printAnagramsCollection(Collection<String> anagrams) {
        System.out.println(getCollectionAsString(anagrams));
    }

    private String getCollectionAsString(Collection<String> anagrams) {
        return Arrays.toString(anagrams.toArray());
    }
}