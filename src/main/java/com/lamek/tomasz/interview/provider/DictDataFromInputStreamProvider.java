package com.lamek.tomasz.interview.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class DictDataFromInputStreamProvider implements DictDataProvider {

    public static final String NULL_INPUT_STREAM = "Input stream can't be null";
    private final String encoding;
    private List<String> words;

    public DictDataFromInputStreamProvider(String encoding) {
        this.encoding = encoding;
    }

    protected abstract InputStream getInputStream() throws IOException;

    @Override
    public Iterator<String> iterator() {

        prepareWordsListIfNotExist();
        return words.iterator();
    }

    private void prepareWordsListIfNotExist() {
        if (words == null) {
            words = new LinkedList<>();
            openStreamAndPrepareWordsList();
        }
    }

    private void openStreamAndPrepareWordsList() {
        try (BufferedReader bufferedReader = getBufferedReader()) {
            fillWordsListFromStream(bufferedReader);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void fillWordsListFromStream(BufferedReader bufferedReader) throws IOException {
        String word;
        while ((word = bufferedReader.readLine()) != null) {
            addWordIfNotEmpty(word);
        }
    }

    private void addWordIfNotEmpty(String word) {
        if (!word.isEmpty())
            words.add(word);
    }

    private BufferedReader getBufferedReader() throws IOException {
        return new BufferedReader(
                getInputStreamReader());
    }

    private InputStreamReader getInputStreamReader() throws IOException {
        InputStream inputStream = getInputStream();
        validateInputStream(inputStream);
        return new InputStreamReader(inputStream, Charset.forName(encoding).newDecoder());
    }

    private void validateInputStream(InputStream inputStream) {
        if (inputStream == null) {
            throw new IllegalStateException(NULL_INPUT_STREAM);
        }
    }
}
