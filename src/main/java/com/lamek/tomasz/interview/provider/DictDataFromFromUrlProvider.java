package com.lamek.tomasz.interview.provider;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DictDataFromFromUrlProvider extends DictDataFromInputStreamProvider {

    private String dictUrl;

    public DictDataFromFromUrlProvider(String dictUrl, String encoding) {
        super(encoding);
        this.dictUrl = dictUrl;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        URL url = new URL(dictUrl);
        return url.openStream();
    }
}
