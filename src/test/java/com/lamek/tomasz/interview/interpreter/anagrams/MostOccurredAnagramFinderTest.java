package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static com.lamek.tomasz.interview.interpreter.anagrams.AnagramDictWordConsumerStubFactory.MOST_OCCURRED_ANAGRAM;
import static com.lamek.tomasz.interview.interpreter.anagrams.AnagramDictWordConsumerStubFactory.prepareAnagramDictWordConsumer;
import static com.lamek.tomasz.interview.interpreter.anagrams.AnagramDictWordConsumerStubFactory.prepareEmptyAnagramDictWordConsumer;

class MostOccurredAnagramFinderTest {
    private MostOccurredAnagramFinder finder;
    private AnagramDictWordConsumer consumer;

    @BeforeEach
    private void init() {
        finder = new MostOccurredAnagramFinder();
        consumer = null;
    }

    @Test
    public void should_returnBiggestSetOfAnagrams() {

        //given
        consumer = prepareAnagramDictWordConsumer();

        //when/then
        Assertions.assertThat(getAnagrams()).containsExactlyInAnyOrder(MOST_OCCURRED_ANAGRAM);
    }

    @Test
    public void should_returnEmptySetOfAnagrams() {

        //given
        consumer = prepareEmptyAnagramDictWordConsumer();

        //when/then
        Assertions.assertThat(getAnagrams()).isEmpty();
    }

    private Collection<String> getAnagrams() {
        return finder.getMostOccurredAnagram(consumer.getAnagrams());
    }
}