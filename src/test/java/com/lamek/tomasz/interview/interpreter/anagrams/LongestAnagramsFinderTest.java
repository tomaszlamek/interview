package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.lamek.tomasz.interview.interpreter.anagrams.AnagramDictWordConsumerStubFactory.*;

class LongestAnagramsFinderTest {

    LongestAnagramsFinder finder = null;
    AnagramDictWordConsumer consumer = null;

    @BeforeEach
    public void init() {
        finder = new LongestAnagramsFinder();
        consumer = null;
    }

    @Test
    public void should_returnBiggestSetOfAnagrams() {

        //given
        consumer = prepareAnagramDictWordConsumer();

        //when/then
        Assertions.assertThat(finder.getBiggestAnagramsCollection(consumer)).containsExactlyInAnyOrder(LONGEST_ANAGRAM);
    }

    @Test
    public void should_returnEmptySetOfAnagrams() {

        //given
        consumer = prepareEmptyAnagramDictWordConsumer();

        //when/then
        Assertions.assertThat(finder.getBiggestAnagramsCollection(consumer)).isEmpty();
    }
}