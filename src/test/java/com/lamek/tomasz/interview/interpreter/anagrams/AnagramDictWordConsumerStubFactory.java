package com.lamek.tomasz.interview.interpreter.anagrams;

import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class AnagramDictWordConsumerStubFactory {

    public static final String[] LONGEST_ANAGRAM = {"abcd", "cbad"};
    public static final String[] MOST_OCCURRED_ANAGRAM = {"abd", "dba", "bda"};

    static AnagramDictWordConsumer prepareAnagramDictWordConsumer() {
        return new AnagramDictWordConsumer() {
            @Override
            public Collection<? extends Collection<String>> getAnagrams() {
                return prepareAnagrams();
            }

            @Override
            public void consumeWord(String word) {

            }
        };
    }

    static AnagramDictWordConsumer prepareEmptyAnagramDictWordConsumer() {
        return new AnagramDictWordConsumer() {
            @Override
            public Collection<? extends Collection<String>> getAnagrams() {
                return new LinkedList<>();
            }

            @Override
            public void consumeWord(String word) {

            }
        };
    }

    static Collection<? extends Collection<String>> prepareAnagrams() {
        List<List<String>> anagrams = new LinkedList<>();
        anagrams.add(Arrays.asList(LONGEST_ANAGRAM));
        anagrams.add(Arrays.asList(MOST_OCCURRED_ANAGRAM));
        return anagrams;
    }
}