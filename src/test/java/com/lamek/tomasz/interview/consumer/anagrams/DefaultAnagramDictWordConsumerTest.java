package com.lamek.tomasz.interview.consumer.anagrams;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAnagramDictWordConsumerTest {

    private DefaultAnagramDictWordConsumer consumer;

    //given
    @BeforeEach
    private void init() {
        consumer = new DefaultAnagramDictWordConsumer();
    }

    @Test
    public void should_returnNothingIfNoAnagramWillBeConsumed() {

        //when
        consumer.consumeWord("aaa");

        //then
        assertThat(consumer.getAnagrams()).isEmpty();
    }


    @Test
    public void should_returnOneAnagramCollectionWithTwoElements() {

        //when
        consumer.consumeWord("abc");
        consumer.consumeWord("cba");

        //then
        assertThat(consumer.getAnagrams()).hasSize(1);

        Collection<String> anagram = consumer.getAnagrams().iterator().next();
        assertThat(anagram).containsExactlyInAnyOrder("abc", "cba");
    }

    @Test
    public void should_returnNothingIfNoWordWillBeConsumed() {

        //then
        assertThat(consumer.getAnagrams()).isEmpty();
    }

    @Test
    public void should_omitEmptyWord() {

        //when
        consumer.consumeWord("");

        //then
        assertThat(consumer.getAnagrams()).isEmpty();
    }

    @Test
    public void should_omitNullValue() {

        //when
        consumer.consumeWord(null);

        //then
        assertThat(consumer.getAnagrams()).isEmpty();
    }
}