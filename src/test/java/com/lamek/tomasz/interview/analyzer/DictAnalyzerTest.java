package com.lamek.tomasz.interview.analyzer;

import com.lamek.tomasz.interview.consumer.DictWordConsumer;
import com.lamek.tomasz.interview.consumer.anagrams.AnagramDictWordConsumer;
import com.lamek.tomasz.interview.provider.DictDataFromArrayProviderStubFactory;
import com.lamek.tomasz.interview.provider.DictDataProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class DictAnalyzerTest {

    private DictAnalyzer dictAnalyzer;
    private DictWordConsumer consumer;
    private DictDataProvider provider;

    //given
    @BeforeEach
    private void init() {
        dictAnalyzer = new DictAnalyzer();
        consumer = mockDictLineConsumer();
        provider = DictDataFromArrayProviderStubFactory.prepareDataProvider("aaa", "bbb");
    }

    @Test
    public void should_throwException_when_inputStreamProviderNotSet() {

        //given
        dictAnalyzer.setWordConsumer(consumer);

        //then
        Assertions.assertThrows(IllegalStateException.class, () -> dictAnalyzer.analyzeDict());
    }

    @Test
    public void should_throwException_when_lineConsumerNotSet() {

        //given
        dictAnalyzer.setDictDataProvider(provider);

        //then
        Assertions.assertThrows(IllegalStateException.class, () -> dictAnalyzer.analyzeDict());
    }

    @Test
    public void should_callConsumeMethodForAllWordsSeparatedByNewLine() throws IOException {

        //given
        setupAnalyzer();

        //when
        dictAnalyzer.analyzeDict();

        //then
        Mockito.verify(consumer).consumeWord("aaa");
        Mockito.verify(consumer).consumeWord("bbb");
    }

    private void setupAnalyzer() {
        dictAnalyzer.setWordConsumer(consumer);
        dictAnalyzer.setDictDataProvider(provider);
    }

    private DictWordConsumer mockDictLineConsumer() {
        return Mockito.mock(AnagramDictWordConsumer.class);
    }

}