package com.lamek.tomasz.interview.provider;

import java.util.Arrays;

public class DictDataFromArrayProviderStubFactory {
    public static DictDataProvider prepareDataProvider(String... elements) {
        return () -> Arrays.asList(elements).iterator();
    }
}
