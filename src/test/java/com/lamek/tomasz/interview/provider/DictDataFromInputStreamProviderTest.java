package com.lamek.tomasz.interview.provider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

public class DictDataFromInputStreamProviderTest {

    DictDataFromInputStreamProvider provider;

    @BeforeEach
    public void init() throws IOException {
        provider = null;
    }

    @Test
    public void should_provideCorrectCollectionOfWords() {

        //given
        prepareProvider("abc\ncba");

        //when/then
        assertThat(provider).containsExactlyInAnyOrder("abc", "cba");
    }

    @Test
    public void should_omitEmptyLines() {

        //given
        prepareProvider("\n\n\n\n");

        //when/then
        assertThat(provider).isEmpty();
    }

    @Test
    public void should_throwExceptionWhenInputStreamIsNull() {

        //given
        prepareProviderWithoutInputStream();

        //when/then
        Assertions.assertThrows(IllegalStateException.class, () -> provider.iterator());
    }

    private void prepareProviderWithoutInputStream() {
        provider = new DictDataFromInputStreamProvider(getDefaultCharset()) {
            @Override
            protected InputStream getInputStream() throws IOException {
                return null;
            }
        };
    }

    private String getDefaultCharset() {
        return Charset.defaultCharset().name();
    }

    private void prepareProvider(String streamInput) {
        provider = new DictDataFromInputStreamProvider(getDefaultCharset()) {
            @Override
            protected InputStream getInputStream() throws IOException {
                return prepareInputStream(streamInput);
            }
        };
    }

    private InputStream prepareInputStream(String stringToStream) throws UnsupportedEncodingException {
        byte[] bytes = stringToStream.getBytes(getDefaultCharset());
        return new ByteArrayInputStream(bytes);
    }
}